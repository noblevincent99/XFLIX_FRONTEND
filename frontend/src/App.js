import './App.css';
import { Switch, Route, } from 'react-router-dom'
import HomePage from './XFlixDashboard/HomePage';
import VideoPage from './XFlixDashboard/VideoPage'


function App() {
  return (<>
    <Switch>
      <Route exact path='/' component={HomePage} />
      <Route path='/video/:id' component={VideoPage} />
    </Switch>
  </>)

}

export default App;
