import './VideoPage.css';
import Header from './Components/Header'
import React from 'react'
import { useParams } from 'react-router-dom'
import { useEffect, useState } from 'react';
import getVideoId from './APIBackend/getVideoId';
import patchVideosVote from './APIBackend/patchVideosVote'
import Dashboard from './Components/Dashboard'
import getVideos from './APIBackend/getVideos'

const CalculateUploadAge = (upload) => {
    if (upload === undefined) return
    const months = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"]
    const milliConst = 86400000
    let strDate = upload.split(" ")
    let now = Date.now()
    let uDate = new Date(strDate[2],months.indexOf(strDate[1]) + 1,strDate[0])
    let duration = (now - uDate)/milliConst
    if (duration >= 365) {
        return Math.floor(duration/365) + " year" + (Math.floor(duration/365) > 1 ? "s" : "")  + " ago"
    } else if (duration >= 30) {
        return Math.floor(duration/30) + " month" + (Math.floor(duration/30) > 1 ? "s" : "")  + " ago"
    } else {
        return Math.floor(duration) + " day" + (Math.floor(duration) > 1 ? "s" : "")  + " ago"
    }
}

const VideoPage = (props) => {
    const [videoObj, setVideoObj] = useState({
        votes: {
            upVotes: 0,
            downVotes: 0
        },
        previewImage: "",
        viewCount: 0,
        videoLink: "",
        title: "",
        genre: "",
        contentRating: "",
        releaseDate: "",
        _id: ""
    })
    const [videos, setVideos] = useState([])
    const searchObject = {
        SearchTitle : "",
        SortQuery : "",
        Genre : [],
        Ratings : ""
    }
    const Id = useParams().id

    useEffect(() => {
        getVideoId({"Id":Id}).then(resp => setVideoObj(resp))
        getVideos(searchObject).then(resp => setVideos(resp))
    },[])

    const handeVoteChange = (arg) => {
        const video = JSON.parse(JSON.stringify(videoObj))
        if (arg.vote === "upVote") video.votes["upVotes"]++
        if (arg.vote === "downVote") video.votes["downVotes"]++
        setVideoObj(video)
        patchVideosVote(arg)
    }

    return (
        <React.Fragment>
            <div className="top-header-section">
                <Header />
            </div>
            <div className="bot-video-section">
                {videoObj.title !== undefined ? <div className="video-body">
                    <iframe className="video-frame" title={videoObj.title} src={"https://" + videoObj.videoLink} />
                    <div className="video-header" > {videoObj.title} </div>
                    <div className="video-desc"> +{videoObj.viewCount} <span>&#8226;</span> {CalculateUploadAge(videoObj.releaseDate)} </div>
                    <div className="vote-buttons">
                        <button className="rating-pill" onClick={() => handeVoteChange({"vote":"upVote","Id":videoObj._id})}>
                            <i className="fa fa-thumbs-o-up" />
                            {videoObj.votes.upVotes > 1000 ? Math.round(videoObj.votes.upVotes / 100) / 10 : videoObj.votes.upVotes} 
                        </button>
                        <button className="rating-pill" onClick={() => handeVoteChange({"vote":"downVote","Id":videoObj._id})}> 
                            <i className="fa fa-thumbs-down" />
                            {videoObj.votes.downVotes > 1000 ? Math.round(videoObj.votes.downVotes / 100) / 10 : videoObj.votes.downVotes} 
                        </button>
                    </div>
                    <hr className="solid" />
                </div> : ""}
                <div className="more-videos-dashboard">
                    <Dashboard videos={videos} />
                </div>
            </div>
        </React.Fragment>
    )
}

export default VideoPage;