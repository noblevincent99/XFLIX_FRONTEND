import axios from 'axios'
import config from '../../index'

const patchVideosVote = (props) => {
    const requestBody = {
        "vote" : props.vote,
        "change" : "increase"
    }
    axios.patch(config.APIEndpoint + '/v1/videos/' + props.Id + "/votes", requestBody)
        .then(resp => "Vote Changed").catch(err => console.log(err))
}

export default patchVideosVote