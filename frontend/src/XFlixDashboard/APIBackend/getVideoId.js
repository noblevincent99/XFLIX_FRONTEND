import axios from 'axios'
import config from '../../index'

const getVideoId = async (props) => {
    const response = axios.get(config.APIEndpoint + '/v1/videos/' + props.Id).then(resp => resp.data)
    return response
}

export default getVideoId;