import axios from 'axios';
import config from '../../index';

const postVideos = async (props) => {
    const monthNames = ["Jan", "Feb", "Mar", "Apr", "May", "Jun",
        "Jul", "Aug", "Sept", "Oct", "Nov", "Dec="];

    const reqObj = props;
    let re = /(\d+)-(\d+)-(\d+)/gi;
    reqObj["releaseDate"] = reqObj["releaseDate"].replace(re,`$3 ${monthNames[new Date(reqObj["releaseDate"])
        .getMonth()]} $1`);
    console.log(reqObj)
    const response = await axios.post( config.APIEndpoint + '/v1/videos', reqObj ).then(resp => resp)
    return response
}

export default postVideos;