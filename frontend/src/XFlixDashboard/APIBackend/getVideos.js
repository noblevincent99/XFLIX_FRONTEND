import axios from 'axios'
import config from '../../index'

const getVideos = async (props) => {
    let response = null
    if (props.SearchTitle === "" && props.SortQuery === "" && props.Genre.length === 0 && props.Ratings === "") 
    {
        response = await axios.get(config.APIEndpoint + '/v1/videos').then(resp => resp.data)
    }
    else 
    {
        const QueryArray = []
        QueryArray.push(props.SearchTitle !== "" ? `title=` + encodeURIComponent(props.SearchTitle): "")
        QueryArray.push(props.SortQuery !== "" ? `sortBy=` + props.SortQuery : "")
        QueryArray.push(props.Genre.length > 0 ? `genres=` + props.Genre.reduce((prev, curr, i) => prev + (i > 0 ? ',' : '') + curr) : "")
        QueryArray.push(props.Ratings !== "" ? `contentRating=` + encodeURIComponent(props.Ratings) : "")
        const QueryString = QueryArray.reduce((prev, curr) => prev + (prev !== "" && curr !== "" ? "&" : "") + curr)
        response = await axios.get(config.APIEndpoint + '/v1/videos?' + QueryString).then(resp => resp.data)
    }
    return response.videos
    // Include exceptions for invalid (i) genre (ii) invalid sort by & (iii) invalid rating
}

export default getVideos