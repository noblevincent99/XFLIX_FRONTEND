import './HomePage.css'
import Header from './Components/Header'
import GenrePanel from './Components/GenrePanel'
import Dashboard from './Components/Dashboard'
import React from 'react'
import { useState, useEffect } from 'react'
import getVideos from './APIBackend/getVideos'

const HomePage = () => {
    const [videos, setVideos] = useState([])
    const [searchObject, setSearchObject] = useState({
            SearchTitle : "",
            SortQuery : "",
            Genre : [],
            Ratings : ""
        })

    const handleChangeSearch = (searchQueries) => {
        setSearchObject(searchQueries)
        getVideos(searchObject).then(resp => setVideos(resp))
    }

    useEffect(() => {
        getVideos(searchObject).then(resp => setVideos(resp))
    },[])

    return (
        <React.Fragment>
            <div className="top-section">
                <Header searchObject={searchObject} handleChangeSearch={handleChangeSearch} altVideo/>
                <GenrePanel searchObject={searchObject} handleChangeSearch={handleChangeSearch} />
            </div>
            <div className="bot-section">
                <Dashboard videos={videos} />
            </div>
        </React.Fragment>
    )
}

export default HomePage;