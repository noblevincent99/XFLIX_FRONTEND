import './Dashboard.css';
import { Container, Row, Col, Card } from 'react-bootstrap'
import { Link } from 'react-router-dom'

const CalculateUploadAge = (upload) => {
    const months = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"]
    const milliConst = 86400000
    let strDate = upload.split(" ")
    let now = Date.now()
    let uDate = new Date(strDate[2],months.indexOf(strDate[1]) + 1,strDate[0])
    let duration = (now - uDate)/milliConst
    if (duration >= 365) {
        return Math.floor(duration/365) + " year" + (Math.floor(duration/365) > 1 ? "s" : "")  + " ago"
    } else if (duration >= 30) {
        return Math.floor(duration/30) + " month" + (Math.floor(duration/30) > 1 ? "s" : "")  + " ago"
    } else {
        return Math.floor(duration) + " day" + (Math.floor(duration) > 1 ? "s" : "")  + " ago"
    }
}

const Dashboard = (props) => {
    return (<> 
        <Container>
            <Row>
                {props.videos.map(item => {
                    return (
                        <Col key={item._id} xl={3} lg={4} md={6} sm={12} className="video-col">
                            <Link to={"/video/" + item._id} style={{textDecoration:"none"}} className="video-tile-link">
                                <Card className="video-card video-tile">
                                    <Card.Img variant="top" src={item.previewImage} />
                                    <Card.Body className="video-card-body">
                                        <Card.Text style={{fontSize: "18px",marginBottom:"2px"}}> {item.title} </Card.Text>
                                        <Card.Text style={{"color":"#BEBEBE"}}> {CalculateUploadAge((item.releaseDate ? item.releaseDate : item.uploadDate))} </Card.Text>
                                    </Card.Body>
                                </Card>
                            </Link>
                        </Col>)
                })}
            </Row>
        </Container>
    </>)
}

export default Dashboard;