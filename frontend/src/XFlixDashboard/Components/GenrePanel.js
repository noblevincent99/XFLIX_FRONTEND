import './GenrePanel.css';
import { useState } from 'react';
import { Form } from 'react-bootstrap'

const GenrePanel = (props) => {
    const genreFilter = ['All genres', 'Education', 'Sports', 'Comedy', 'Lifestyle']
    const ageFilter = ['Any age group', '7+', '12+', '16+', '18+']

    const [genre, setGenre] = useState(new Map())
    const [age, setAge] = useState("")

    const handleGenreChange = (filter) => {
        if (filter === genreFilter[0] || genre.has(genreFilter[0])) {
            setGenre(new Map(genre.clear()))
            setGenre(new Map(genre.set(filter, 1)))
        } 
        else if (genre.has(filter)) {
            genre.delete(filter)
            setGenre(new Map(genre))
        } 
        else {
            setGenre(new Map(genre.set(filter, 1)))
        }
        const SearchObject = props.searchObject
        if (filter === genreFilter[0]) SearchObject.Genre = []
        else SearchObject.Genre = [...genre.keys()]
        props.handleChangeSearch(SearchObject)
    }

    const handleAgeChange = (filter) => {
        setAge(filter)
        const SearchObject = props.searchObject
        if (filter === ageFilter[0]) SearchObject.Ratings = ""
        else SearchObject.Ratings = filter
        props.handleChangeSearch(SearchObject)
    }

    const handleSortChange = (event) => {
        const SearchObject = props.searchObject
        SearchObject.SortQuery = event.target.value
        props.handleChangeSearch(SearchObject)
    }

    return ( <div className="filters">
        <div className="filters-container genre-filter">
            {genreFilter.map(filter => {
                return (<button key={filter} onClick={() => handleGenreChange(filter)} className={"pill" + (genre.has(filter) ? " selected" : "") + " genre-btn"}>{filter}</button>)
            })}
        </div>
        <div className="filters-container age-filter">
            {ageFilter.map(filter => {
                return (<button key={filter} onClick={() => handleAgeChange(filter)} className={"pill" + (age === filter ? " selected" : "") + " content-rating-btn"}>{filter}</button>)
            })}
        </div>
        <div className="sort-bar">
        <Form.Select 
            className="sort-select"
            aria-label="Default select example" 
            onChange={(event) => handleSortChange(event)}>
            <option value="releaseDate" id="release-date-option" selected>Uploaded Date</option>
            <option value="viewCount" id="view-count-option">View Count</option>
        </Form.Select>
        </div>
    </div> ) 
}

export default GenrePanel;