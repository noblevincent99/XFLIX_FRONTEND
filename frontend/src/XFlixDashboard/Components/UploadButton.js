import './UploadButton.css'
import { Button, TextField, Dialog, DialogActions, DialogContent, FormHelperText, DialogTitle, InputLabel, MenuItem, FormControl,Select } from '@mui/material';
import { withStyles } from '@mui/styles'

const styles = {
    root: {                        
        '& .MuiOutlinedInput-root': {  
            '& fieldset': {            
                borderColor: '#999999',   
            },
            '&:hover fieldset': {
                borderColor: '#999999', 
            },
            '&.Mui-focused fieldset': {  
                borderColor: '#999999',
            },
            color: '#999999'
        },
        '& .MuiInputLabel-root' : {
            color: '#999999',
        },
        '& .MuiFormHelperText-root': {
            color: '#999999',
        }
    },
};

const CSSTextField = withStyles(styles)(function(props){
    const { classes, formObj } = props;
    return ( 
        <TextField 
            required fullWidth 
            margin="dense" 
            size="small" 
            variant="outlined" 
            id={props.id} 
            label={props.label} 
            type={props.type} 
            helperText={props.helperText} 
            classes={ classes }
            value={ formObj[props.id] }
            onChange={(event) => props.onChange({field:props.id, value:event.target.value})}
        /> 
    )
})

const CSSSelectForm = withStyles(styles)(function (props) {
    const { helperText, id, label, optionsArray, classes, formObj } = props;
    return (
    <FormControl fullWidth size="small" margin="dense" classes={ classes }>
        <InputLabel id="id">{label}</InputLabel>
        <Select 
            labelId={id} 
            id={id} 
            label={label} 
            value={formObj[id]} 
            onChange={(event) => props.onChange({field:props.id,value:event.target.value})}>
            { optionsArray.map(item => <MenuItem key={item} value={item}>{item}</MenuItem>) }
        </Select>
        <FormHelperText>{ helperText }</FormHelperText>
    </FormControl>)
})

const UploadButton = (props) => {

    const genreFilter = ['Education', 'Sports', 'Comedy', 'Lifestyle']
    const ageFilter = ['7+', '12+', '16+', '18+']

    return (
        <Dialog open={props.open} onClose={props.handleClose}>
            <div className="dialog-box">
                <DialogTitle sx={{fontSize:"25px", fontWeight:"700", color:"#8b8a8a"}}>Upload Video</DialogTitle>
                <DialogContent>
                    <CSSTextField 
                        id="videoLink"
                        label="Video Link"
                        type="url"
                        helperText="This link will be used to derive the video"
                        onChange={props.formChange}
                        formObj={props.formObj}
                    /> 
                    <CSSTextField 
                        id="previewImage"
                        label="Thumbnail Image Link"
                        type="url"
                        helperText="This link will be used to preview the thumbnail image"
                        onChange={props.formChange}
                        formObj={props.formObj}
                    />
                    <CSSTextField 
                        id="title"
                        label="Title"
                        type="text"
                        helperText="The title will be the representative text for video"
                        onChange={props.formChange}
                        formObj={props.formObj}
                    />
                    <CSSSelectForm 
                        optionsArray={genreFilter} 
                        id="genre" 
                        label="Genre" 
                        helperText="Genre will help in categorizing your video" 
                        onChange={props.formChange}
                        formObj={props.formObj}
                    />
                    <CSSSelectForm 
                        optionsArray={ageFilter} 
                        id="contentRating" 
                        label="Age" 
                        helperText="This will help to filter videos on age group suitability" 
                        onChange={props.formChange}
                        formObj={props.formObj}
                    />
                    <CSSTextField
                        id="releaseDate"
                        label="Release Date"
                        type="date"
                        helperText="This will be used to sort the videos"
                        onChange={props.formChange}
                        formObj={props.formObj}
                    />
                </DialogContent>
                <DialogActions sx={{justifyContent: "flex-start", marginBottom:"10px",paddingLeft:"20px"}}>
                    <Button onClick={props.submitVideo} sx={{backgroundColor:"red", color: "white"}} className="submit-button" id="upload-btn-submit"n>UPLOAD VIDEO</Button>
                    <Button onClick={props.handleClose} sx={{color: "white"}} id="upload-btn-cancel">CANCEL</Button>
                </DialogActions>
            </div>
        </Dialog>
    )
}

export default UploadButton;