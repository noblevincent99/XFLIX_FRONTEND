import './Header.css';
import { useState } from 'react';
import Logo from './Logo.png';
import { Button } from '@mui/material';
import FileUploadIcon from '@mui/icons-material/FileUpload';
import UploadButton from './UploadButton';
import postVideos from '../APIBackend/postVideos';
import { useSnackbar } from 'notistack';

const Header = (props) => {
    const [search, setSearch] = useState("");
    const [open, setOpen] = useState(false);
    const { enqueueSnackbar } = useSnackbar();
    const [uploadForm, setUploadForm] = useState({
        videoLink: "",
        previewImage: "",
        title: "",
        genre: "",
        contentRating: "",
        releaseDate: ""
    })

    const onChangeSearchBar = (event) => {
        event.preventDefault()
        const SearchObject = props.searchObject
        SearchObject.SearchTitle = search
        props.handleChangeSearch(SearchObject)
    }

    const onUploadFormChange = (props) => {
        const { field, value } = props
        const uploadObj = JSON.parse(JSON.stringify(uploadForm))
        uploadObj[field] = value
        setUploadForm(uploadObj)
    }

    const validateVideoObject = (props) => {
        if (props.videoLink === "") {
            enqueueSnackbar('Enter the video link!',{variant: 'warning'})
            return false
        } 
        const REGEX = /youtube.com\/embed\/[\w\d]+/gi
        if (!REGEX.test(props.videoLink)) {
            enqueueSnackbar('Video link must be in format, eg. youtube.com/embed/<video-specific-identifier>',{variant:"error"})
            return false
        }
        if (props.previewImage === "") {
            enqueueSnackbar('Enter the thumbnail image link!',{variant: 'warning'})
            return false
        } 
        if (props.title === "") {
            enqueueSnackbar('Enter the title!',{variant: 'warning'})
            return false
        } 
        if (props.genre === "") {
            enqueueSnackbar('Enter the genre!',{variant: 'warning'})
            return false
        } 
        if (props.contentRating === "") {
            enqueueSnackbar('Enter the content rating!',{variant: 'warning'})
            return false
        }
        if (props.releaseDate === "") {
            enqueueSnackbar('Enter the release date!',{variant: 'warning'})
            return false
        } 
        return true
    }

    const onUploadVideoSubmit = () => {
        if (validateVideoObject(uploadForm)){ 
            postVideos(uploadForm).then(resp => {
                if (resp.status === 201){
                    enqueueSnackbar('Video uploaded successfully!',{variant: 'success'})
                    setOpen(false)
                    setUploadForm({
                        videoLink: "",
                        previewImage: "",
                        title: "",
                        genre: "",
                        contentRating: "",
                        releaseDate: ""
                    })
                }
            })
        }
    }

    return (<div className="header">
        <img src={Logo} alt="Logo" className="Logo"></img>
        {props.altVideo ? <> <form className="search-form" >
            <input type="search" className="search-field" placeholder="Search" onChange={(event) => setSearch(event.target.value)}/>
            <input type="submit" className="search-logo" onClick={onChangeSearchBar} />
        </form>
        <Button variant="contained" id="upload-btn" size="small" onClick={() => setOpen(true)} className="upload-button" startIcon={<FileUploadIcon />}>Upload</Button> 
        <UploadButton 
            formObj={uploadForm} 
            open={open} 
            handleClickOpen={() => setOpen(true)} 
            handleClose={() => {
                setUploadForm({
                    videoLink: "",
                    previewImage: "",
                    title: "",
                    genre: "",
                    contentRating: "",
                    releaseDate: ""
                })
                setOpen(false)
            }} 
            formChange={onUploadFormChange}
            submitVideo={onUploadVideoSubmit}
        />
        </>
        : <> </>}
    </div>)
}

export default Header