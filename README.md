<h2>Overview</h2>
<p>XFlix is a video sharing platform which hosts videos for the world to watch. It also features uploading new videos by using external video links (eg: Youtube) and playing these videos.</p>
During the course of this project,

- Built XFlix frontend using React.js from scratch
- Utilized the figma file to understand the design requirements
- Used Postman collection to understand API requirements
- Mocked backend server using Postman Mock Server for API responses 

<h3>Build XFlix Frontend completely from scratch</h3>
<h4>Scope of Work</h4>

- Implemented 3 views - `Landing page`, `Video page`, `Video Upload Modal` - in alignment to the Figma design
- Used Postman collection to understand the API requirements for 5 different REST APIs
- Utilized Postman Mock Server to check working of the Frontend application end-to-end
- Deployed the React application to Netlify

<b>Skills used - </b> <br/>
`React.js` `Figma` `Postman` `REST API` `Netlify`

